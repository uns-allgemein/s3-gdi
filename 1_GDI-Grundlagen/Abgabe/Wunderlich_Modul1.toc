\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Architekturmodell einer GDI}{3}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Vor und Nachteile einer GDI}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Beschreibung von INSPIRE}{8}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Webportale}{9}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Webportal: Denkmalschutz}{9}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Allgemeines}{9}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Fragestellung 1}{9}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Fragestellung 2}{10}{subsection.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Webportal: Naturschutz}{11}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Allgemeines}{11}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Fragestellung 1}{11}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Fragestellung 2}{12}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Fragestellung 3}{13}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Webportal: Regionalverband}{14}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Allgemeines}{14}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.2}Fragestellung 1}{14}{subsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.3}Fragestellung 2}{15}{subsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.4}Fragestellung 3}{15}{subsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Webportal: Radrouten}{16}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Allgemeines}{16}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.2}Fragestellung 1}{16}{subsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Webportal: COVID-19-Dashboard}{17}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}Allgemeines}{17}{subsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.2}Fragestellung 1}{17}{subsection.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Webportal: M\IeC {\"a}ngelmelder Frankfurt am Main}{18}{section.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.1}Allgemeines}{18}{subsection.4.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.2}Fragestellung 1}{18}{subsection.4.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.3}Fragestellung 2}{18}{subsection.4.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6.4}Fragestellung 3}{18}{subsection.4.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Feedback}{19}{chapter.5}
