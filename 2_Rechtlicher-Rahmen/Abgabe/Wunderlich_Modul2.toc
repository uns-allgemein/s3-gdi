\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Fragestellungen im Text}{3}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Aufbau der INSPIRE Richtlinie}{3}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Was ist eine Geodateninfrastruktur?}{3}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.2}Aus welchen Kapiteln besteht die Richtlinie?}{3}{subsection.1.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.3}Gibt es Zugangsbeschr\IeC {\"a}nkungen f\IeC {\"u}r die Datens\IeC {\"a}tze?}{3}{subsection.1.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.4}Sind die Datens\IeC {\"a}tze kostenfrei?}{3}{subsection.1.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.5}Wie wird die GDI koordiniert?}{4}{subsection.1.1.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.6}Was regeln die Schlussbestimmungen?}{4}{subsection.1.1.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Welche Institutionen sind der gdi.initiative.sachsen beigetreten?}{4}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Vergleich der GDI-Projekte von Sachsen und Hessen}{4}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Durchf\IeC {\"u}hrungsbestimmungen}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Betroffene Geodaten}{7}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}S\IeC {\"a}chsischen Geodateninfrastrukturgesetz}{8}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Der Aufbau}{8}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Regelungen der Metadaten}{8}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Feedback}{9}{chapter.5}
