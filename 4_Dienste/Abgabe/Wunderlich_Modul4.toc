\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{2}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Profil eines Dienstes}{2}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Aufgabe, Operatoren und Parameter von WMS-Diensten}{3}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Operatoren}{3}{subsection.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}GetCapabilities}{3}{subsubsection.3.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}GetMap}{5}{subsubsection.3.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.3}GetFeatureInfo}{6}{subsubsection.3.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Orchestrierung von Diensten}{7}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Registry im Rahmen einer GDI}{8}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Übung}{9}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Teil 1 - Einbinden zweier WMS in ArcGIS}{9}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.1}Können Sie Objekte selektieren sowie deren Geometrie und Symbologie ändern?}{9}{subsubsection.6.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Teil 2 - WMS Operatoren}{10}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.1}GetCapabilities-Request - Vergleichen Sie die dargestellten Informationen mit denen des XML.}{10}{subsubsection.6.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.2}GetMap-Request - Änderungen an dem Aufruf}{11}{subsubsection.6.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.3}GetFeatureInfo-Request - Abfragen von FeatureInfos}{11}{subsubsection.6.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.4}Was ist die Antwort auf eine GetCapabilities-Anfrage}{13}{subsubsection.6.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.5}Was ist die Antwort auf eine GetCapabilities-Anfrage}{13}{subsubsection.6.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Teil 3 - WMS in Google Earth}{13}{subsection.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Teil 4 - Einbinden zweier WMS in ArcGIS - Vertiefung}{14}{subsection.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Feedback}{15}{section.7}%
