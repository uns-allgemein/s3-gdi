\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{2}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Aufgaben der OGC und deren Mitglieder}{2}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Beschreibung von 3D-Tiles}{4}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}ISO und WFS-Standard}{6}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Mitgliedschaften in der ISO}{6}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Übung}{7}{section.6}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Teil 1 - Geoportal Hessen}{7}{subsection.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Teil 2 - Landschaftsinformationssystem RLP}{10}{subsection.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Teil 3 - Geoportal.de}{11}{subsection.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Feedbeck}{12}{section.7}%
